// Re-implementation of
// openssl enc -aes-128-cbc -in test -out test.enc -K <key> -iv <iv>


extern crate openssl;
extern crate hex;
extern crate error_chain;

#[macro_use]
extern crate structopt;
extern crate openssl_test;

use openssl_test::errors::*;

use openssl::*;
use structopt::StructOpt;

#[structopt(name = "openssl-test")]
#[derive(StructOpt, Debug)]
struct Opt {
    /// key
    #[structopt(short = "k", long = "key")]
    key: String,

    /// iv
    #[structopt(short = "i", long = "iv")]
    iv: String,

    /// message
    #[structopt(short = "m", long = "msg")]
    msg: String,
}


fn encrypt(aes_key: &[u8], aes_iv: &[u8], msg: &[u8]) -> Result<Vec<u8>> {
    let aes_cipher = symm::Cipher::aes_128_cbc();

    let ciphertext = symm::encrypt(
        aes_cipher,
        aes_key,
        Some(&aes_iv),
        msg).chain_err(|| "symm::encrypt failed")?;

    println!("Msg: {}", hex::encode(&msg));
    println!("Key: {}", hex::encode(&aes_key));
    println!("IV: {}", hex::encode(&aes_iv));
    println!("Cipher text: {}", hex::encode(&ciphertext));

    Ok(ciphertext)
}

fn run() -> Result<()> {
    let opt = Opt::from_args();
    println!("{:?}", opt);

    let res = encrypt(&opt.key.as_bytes(), &opt.iv.as_bytes(), &opt.msg.as_bytes()).chain_err(|| "failed to encrypt")?;
    println!("Res is {:?}", res);
    Ok(())
}

fn main() {
    if let Err(ref e) = run() {
        println!("error: {}", e);
        for e in e.iter().skip(1) {
            println!("caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            println!("backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

