#![recursion_limit = "1024"]
#[macro_use]
extern crate error_chain;
extern crate openssl;

pub mod errors;