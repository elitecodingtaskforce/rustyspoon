error_chain! {
    foreign_links {
        OpenSSL(::openssl::error::Error);
        OpenSSLStack(::openssl::error::ErrorStack);
    }

    errors {
        TestError(info: String) {
            description("Test error")
            display("{}", info)
        }
    }
}
