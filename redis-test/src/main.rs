

extern crate redis;

use redis::Commands;

fn tasking_init(con: &redis::Connection, pubsub: &mut redis::PubSub) {
    pubsub.psubscribe("__keyspace@0__:TASK_*");
    let existing_keys: Vec<String> = con.keys("TASK_*").unwrap();

    for key in &existing_keys {
        println!("Tasking event: {}", key);
    }
}

fn tasking_event(key: &String) {
    println!("Tasking event: {}", key);
}

fn main() {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let con = client.get_connection().unwrap();
    let mut pubsub = client.get_pubsub().unwrap();

    tasking_init(&con, &mut pubsub);

    loop {
        let msg = pubsub.get_message().unwrap();
        let payload : String = msg.get_payload().unwrap();
        println!("channel '{}': {}", msg.get_channel_name(), payload);
    }


}