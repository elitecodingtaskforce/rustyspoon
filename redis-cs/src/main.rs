extern crate redis;
extern crate time;
#[macro_use]
extern crate structopt;

use redis::RedisResult;
use redis::Commands;
use structopt::StructOpt;
use std::cmp;

#[derive(StructOpt, Debug)]
#[structopt(name = "redis-test")]
struct Opt {
    /// host
    #[structopt(short = "h", long = "host")]
    host: String,

    /// port
    #[structopt(short = "p", long = "port", default_value = "6379")]
    port: i32,

    /// topic
    #[structopt(short = "t", long = "topic")]
    topic: String,

    /// message
    #[structopt(short = "m", long = "msg")]
    msg: String,
}


fn main() {

    let opt = Opt::from_args();
    println!("{:?}", opt);

    let client = redis::Client::open(format!("redis://{}/", opt.host).as_ref()).unwrap();
    let con = client.get_connection().unwrap();

    if opt.msg.is_empty() {
        let mut pubsub = client.get_pubsub().unwrap();
        pubsub.subscribe(opt.topic).unwrap();

        let mut micro_max: f64 = 0.0;
        let mut micro_min: f64 = 1000.0;
        let mut sum: u64 = 0;
        let mut c = 0;
        loop {
            c += 1;
            let msg = pubsub.get_message().unwrap();
            let cur_time: u64 = time::precise_time_ns();
            let payload : u64 = msg.get_payload().unwrap();
            let delta = cur_time-payload;

            let micro = (delta)/1000;
            sum += micro;

            let milli = micro as f64/1000.0;
            micro_max = micro_max.max(micro as f64);
            micro_min = micro_min.min(micro as f64);
            println!("channel '{}': {} {} {}us {}ms (min: {}, max: {}, avg: {})", msg.get_channel_name(), payload, cur_time, micro, milli, micro_min, micro_max, sum/c);

        }
    } else {
        let cur_time = time::precise_time_ns();
        redis::cmd("PUBLISH")
            .arg(&opt.topic)
            .arg(cur_time)
            .execute(&con);
    }







}
