extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::Parser;

#[derive(Parser)]
#[grammar = "httpresp.pest"]
struct HTTPRespParser;

fn main() {
    let parser_queue = HTTPRespParser::parse(Rule::http_resp, "HTTP/1.1 200 OK\r\nHeader1: val1\r\nHeader2: val2\r\n\r\nmybody").unwrap_or_else(|e| panic!("{}", e));

    for p in parser_queue.flatten().peekable() {
        match p.as_rule() {
            Rule::http_resp => println!("Full response:  \"{}\"\n", p.into_span().as_str()),
            Rule::http_status_line => println!("Status line: \"{}\"", p.into_span().as_str()),
            Rule::http_header => {
                let header_text = p.clone().into_inner().filter(|p| p.as_rule() == Rule::http_header_text).next().unwrap().into_span().as_str();
                let header_value = p.clone().into_inner().filter(|p| p.as_rule() == Rule::http_header_value).next().unwrap().into_span().as_str();
                print!("Header text: \"{}\"", header_text);
                print!(" | ");
                println!("Header value: \"{}\"", header_value);
            },
            Rule::http_body => println!("Body: \"{}\"", p.into_span().as_str()),
            _ => ()
        };
    }
}